import java.io.IOException;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

/**
 * The controller for the otherUser.fxml, where it displays the user and
 * artatawe butons and the details of another user.
 * 
 * @author Anthony Fanias.
 *
 */
public class OtherUsersController {

	@FXML
	Button userLoggedIn;
	@FXML
	Button artataweButton;
	@FXML
	Button favouriteButton;
	@FXML
	ImageView userImage;
	@FXML
	TextArea userInfo;
	@FXML
	Label displayUserName;

	/**
	 * Initialises the login handler.
	 */
	public void initialize() {

		userLoggedIn.setOnAction(e -> {
			handleUserLoggedIn();
		});

		artataweButton.setOnAction(e -> {
			handleArtataweButton();
		});

		favouriteButton.setOnAction(e -> { 
			try {
				handleFavButton();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		});
		
		showProfileImage();
		
		displayInfo();
		
		displayUserName.setText(LogInPageController.u.getUserName());
	}

	/**
	 * Handles the users logged in event. 
	 */
	private void handleUserLoggedIn() {
		try {
			Parent profilePage = FXMLLoader.load(getClass().getResource("profile.fxml"));

			Scene profilePageScene = new Scene(profilePage);

			Stage newStage = (Stage) userLoggedIn.getScene().getWindow();
			newStage.setScene(profilePageScene);
			newStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Handles the Artatawe button event. 
	 */
	private void handleArtataweButton() {
		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("artatawe.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) artataweButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Handle favourite user button
	 * @throws IOException
	 */
	private void handleFavButton() throws IOException {
		LogInPageController.u.addFavUser(FavUsersController.o.getUserName(), FavUsersController.o.getImagePath());
	}

	private void showProfileImage() {
		try {
			Image image = new Image(FavUsersController.o.getImagePath());
			userImage.setImage(image);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void displayInfo() {
		userInfo.setText(FavUsersController.o.getUserInfo());
	}
}
