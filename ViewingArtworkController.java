import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

/**
 * The controller for the viewArtwork.fxml, where you can view an artwork, bid
 * on it or view the user that put it up.
 * 
 * @author Anthony Fanias.
 *
 */
public class ViewingArtworkController {

	@FXML
	Button userLoggedIn;
	@FXML
	Button artataweButton;
	@FXML
	Button placeBidButton;
	@FXML
	Button viewUserButton;
	@FXML
	TextField bid;
	@FXML
	ImageView artworkImage;
	@FXML
	TextArea artworkDescription;
	@FXML
	Label displayUserName;

	/**
	 * Initialize the event handlers.
	 */
	public void initialize() {

		userLoggedIn.setOnAction(e -> {
			handleUserLoggedIn();
		});

		artataweButton.setOnAction(e -> {
			handleArtataweButton();
		});

		placeBidButton.setOnAction(e -> {
			try {
				handleBidButton();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		});

		viewUserButton.setOnAction(e -> {
			handleViewUserButton();
		});

		displayUserName.setText(LogInPageController.u.getUserName());

		displayImage();
		newTime();
		displayInfo();
	}

	/**
	 * 
	 */
	private void handleArtataweButton() {
		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("artatawe.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) artataweButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	private void handleUserLoggedIn() {
		try {
			Parent profilePage = FXMLLoader.load(getClass().getResource("profile.fxml"));

			Scene profilePageScene = new Scene(profilePage);

			Stage newStage = (Stage) userLoggedIn.getScene().getWindow();
			newStage.setScene(profilePageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Handle bid button.
	 * @throws IOException
	 */
	private void handleBidButton() throws IOException {
		FileReader f = new FileReader();
		Bidding b;
		Double newBid = Double.parseDouble(bid.getText());
		
		if (f.readBidding(MainPageController.a.getItemID()) == null){
			b = new Bidding(0.0, 0, MainPageController.a.getItemID(), null, MainPageController.a.getSellerName(), newTime());
		} else {
			b = f.readBidding(MainPageController.a.getItemID());
		}
		b.placeBid(newBid, LogInPageController.u.getUserName());
	}

	/**
	 * Handle view user Button.
	 */
	private void handleViewUserButton() {
		try {
			Parent viewUserPage = FXMLLoader.load(getClass().getResource("otherUser.fxml"));

			Scene viewUserPageScene = new Scene(viewUserPage);

			Stage newStage = (Stage) viewUserButton.getScene().getWindow();
			newStage.setScene(viewUserPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Display artwork image.
	 */
	private void displayImage() {
		try {
			Image image = new Image(MainPageController.a.getPhotoPath());
			artworkImage.setImage(image);
		} catch (Exception e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Get the date time.
	 * @return Date time.
	 */
	private String newTime() {
		LocalDateTime now = LocalDateTime.now();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		String formatDateTime = now.format(formatter);
		return formatDateTime;
	}

	/**
	 * Display artwork info.
	 */
	private void displayInfo() {
		if (MainPageController.a.getType().equals("S")) {
			artworkDescription.setText(MainPageController.s.getInfo());
		} else {
			artworkDescription.setText(MainPageController.p.getInfo());
		}
	}

}
