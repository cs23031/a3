
import javafx.scene.image.Image;

/**
 * 
 * @author Josh Jones, Matt Leonard.
 *
 */

public class Sculpture extends Artworks {
	int dimensionW;
	int dimensionH;
	int dimensionD;
	String material;
	String[] additionalPhotos;

	/**
	 * 
	 * @param itemID
	 * @param title
	 * @param photoPath
	 * @param artistName
	 * @param yearCreated
	 * @param reservePrice
	 * @param bidLimit
	 * @param date
	 * @param bidsPlaced
	 * @param currentPrice
	 * @param sellerName
	 * @param dateSold
	 * @param description
	 * @param dimensionW
	 * @param dimensionH
	 * @param dimensionD
	 * @param material
	 * @param additionalPhotos
	 */
	public Sculpture(int itemID, String title, String photoPath, String artistName, int yearCreated,
			double reservePrice, int bidLimit, String date, int bidsPlaced, double currentPrice, String sellerName,
			String dateSold, String description, int dimensionW, int dimensionH, int dimensionD, String material,
			String[] additionalPhotos) {

		super(itemID, title, photoPath, artistName, yearCreated, reservePrice, bidLimit, date, bidsPlaced, currentPrice,
				sellerName, dateSold, description);

		this.dimensionW = dimensionW;
		this.dimensionH = dimensionH;
		this.dimensionD = dimensionD;
		this.material = material;
		this.additionalPhotos = additionalPhotos;
		this.artworkType = "S";

	}

	/**
	 * 
	 * @param itemID
	 * @param title
	 * @param photoPath
	 * @param artistName
	 * @param yearCreated
	 * @param reservePrice
	 * @param bidLimit
	 * @param date
	 * @param bidsPlaced
	 * @param currentPrice
	 * @param sellerName
	 * @param dateSold
	 * @param dimensionW
	 * @param dimensionH
	 * @param dimensionD
	 * @param material
	 * @param additionalPhotos
	 */
	public Sculpture(int itemID, String title, String photoPath, String artistName, int yearCreated,
			double reservePrice, int bidLimit, String date, int bidsPlaced, double currentPrice, String sellerName,
			String dateSold, int dimensionW, int dimensionH, int dimensionD, String material,
			String[] additionalPhotos) {

		super(itemID, title, photoPath, artistName, yearCreated, reservePrice, bidLimit, date, bidsPlaced, currentPrice,
				sellerName, dateSold);

		this.dimensionW = dimensionW;
		this.dimensionH = dimensionH;
		this.dimensionD = dimensionD;
		this.material = material;
		this.additionalPhotos = additionalPhotos;
		this.artworkType = "S";

	}

	/**
	 * 
	 * @param itemID
	 * @param title
	 * @param photoPath
	 * @param artistName
	 * @param yearCreated
	 * @param reservePrice
	 * @param bidLimit
	 * @param date
	 * @param bidsPlaced
	 * @param currentPrice
	 * @param sellerName
	 * @param dateSold
	 * @param description
	 * @param dimensionW
	 * @param dimensionH
	 * @param dimensionD
	 * @param material
	 */
	public Sculpture(int itemID, String title, String photoPath, String artistName, int yearCreated,
			double reservePrice, int bidLimit, String date, int bidsPlaced, double currentPrice, String sellerName,
			String dateSold, String description, int dimensionW, int dimensionH, int dimensionD, String material) {

		super(itemID, title, photoPath, artistName, yearCreated, reservePrice, bidLimit, date, bidsPlaced, currentPrice,
				sellerName, dateSold, description);

		this.dimensionW = dimensionW;
		this.dimensionH = dimensionH;
		this.dimensionD = dimensionD;
		this.material = material;
		this.artworkType = "S";

	}

	/**
	 * 
	 * @param itemID
	 * @param title
	 * @param photoPath
	 * @param artistName
	 * @param yearCreated
	 * @param reservePrice
	 * @param bidLimit
	 * @param date
	 * @param bidsPlaced
	 * @param currentPrice
	 * @param sellerName
	 * @param dateSold
	 * @param dimensionW
	 * @param dimensionH
	 * @param dimensionD
	 * @param material
	 */
	public Sculpture(int itemID, String title, String photoPath, String artistName, int yearCreated,
			double reservePrice, int bidLimit, String date, int bidsPlaced, double currentPrice, String sellerName,
			String dateSold, int dimensionW, int dimensionH, int dimensionD, String material) {

		super(itemID, title, photoPath, artistName, yearCreated, reservePrice, bidLimit, date, bidsPlaced, currentPrice,
				sellerName, dateSold);

		this.dimensionW = dimensionW;
		this.dimensionH = dimensionH;
		this.dimensionD = dimensionD;
		this.material = material;
		this.artworkType = "S";

	}

	/**
	 * 
	 * @param itemID
	 * @param title
	 * @param photoPath
	 * @param artistName
	 * @param yearCreated
	 * @param reservePrice
	 * @param bidLimit
	 * @param date
	 * @param sellerName
	 * @param dimensionW
	 * @param dimensionH
	 * @param dimensionD
	 * @param material
	 * @param description
	 */
	public Sculpture(int itemID, String title, String photoPath, String artistName, int yearCreated,
			double reservePrice, int bidLimit, String date, String sellerName, int dimensionW, int dimensionH,
			int dimensionD, String material, String description) {

		super(itemID, title, photoPath, artistName, yearCreated, reservePrice, bidLimit, date, sellerName, description);

		this.dimensionW = dimensionW;
		this.dimensionH = dimensionH;
		this.dimensionD = dimensionD;
		this.material = material;
		this.artworkType = "S";
	}

	/**
	 * 
	 * @return
	 */
	public int getDimensionW() {
		return dimensionW;
	}

	/**
	 * 
	 * @return
	 */
	public int getDimensionH() {
		return dimensionH;
	}

	/**
	 * 
	 * @return
	 */
	public int getDimensionD() {
		return dimensionD;
	}

	/**
	 * 
	 * @return
	 */
	public String getMaterial() {
		return material;
	}

	/**
	 * 
	 */
	public String[] getAdditionalPhotos() {
		return additionalPhotos;
	}

	/**
	 * 
	 * @param additionalPhotos
	 */
	public void setAdditionalPhotos(String[] additionalPhotos) {
		this.additionalPhotos = additionalPhotos;
	}

	/**
	 * 
	 */
	public void getPhotosAdded() {
		int i = 0;
		Image extraPhoto;
		while (i < additionalPhotos.length) {
			extraPhoto = new Image(additionalPhotos[i]);
			i++;
			System.out.println(extraPhoto);
		}
		// PRINTS IN BLOCK, could print separate
	}

	/**
	 * 
	 */
	public String toString() {
		String result = super.toString();
		result += "," + this.dimensionW + "," + this.dimensionH + "," + this.dimensionD + "," + this.material;
		if (additionalPhotos != null) {
			for (String s : additionalPhotos) {
				result += "," + s;
			}
		}
		return result;
	}
	
	
	public String getInfo() {
		return "Title: " + this.title + "\n" +
				"Name of Artist: " + this.artistName + "\n" +
				"Year Created: " + this.yearCreated + "\n" +
				"Reserve Price: " + this.reservePrice + "\n" +
				"Date: " + this.date + "\n" +
				"Description: " + this.description + "\n" +
				"Dimensions: " + this.dimensionH + " " + this.dimensionW + " " + this.dimensionD+ "\n" +
				"Material: " + this.material + "\n" +
				"Name of Seller: " + this.sellerName + "\n";
	}
}