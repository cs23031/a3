import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

/**
 * The controller for the profile.fxml, where the user can view all their
 * details.
 * 
 * @author Anthony Fanias, Abdi Mohamed, Matthew Payne.
 *
 */
public class ProfileController {

	@FXML
	Button userLoggedIn;
	@FXML
	Button artataweButton;
	@FXML
	Button drawImageButton;
	@FXML
	Button selectDefaultButton;
	@FXML
	Button favUsersButton;
	@FXML
	Button wonArtworksButton;
	@FXML
	Button soldArtworksButton;
	@FXML
	Button newArtworkButton;
	@FXML
	Button editProfileButton;
	@FXML
	Button logOutButton;
	@FXML
	TextArea infoArea;
	@FXML
	ImageView profileImage;
	@FXML
	Label displayUserName;

	/**
	 * Initialises the event handler.
	 */
	public void initialize() {

		userLoggedIn.setOnAction(e -> {
			// display the user that is logged in + take them to this page
		});

		artataweButton.setOnAction(e -> {
			// take them to the main page
			handleArtataweButton();
		});

		drawImageButton.setOnAction(e -> {
			handleDrawImageButton();
			showProfileImage();
			System.out.println(LogInPageController.u.getImagePath());

		});

		selectDefaultButton.setOnAction(e -> {
			handleDefaultButton();
		});

		favUsersButton.setOnAction(e -> {
			// takes you to the fav users scene
			handleFavUsersButton();
		});

		wonArtworksButton.setOnAction(e -> {
			// takes you to the won artworks scene
			handleWonArtworksButton();
		});

		soldArtworksButton.setOnAction(e -> {
			handleSoldArtworksButton();
		});

		newArtworkButton.setOnAction(e -> {
			handleNewArtworkButton();
		});

		editProfileButton.setOnAction(e -> {
			handleEditProfileButton();
		});

		logOutButton.setOnAction(e -> {
			handleLogOutButton();
		});

		displayUserName.setText(LogInPageController.u.getUserName());
		displayUserInfo();
		showProfileImage();
	}

	/**
	 * provide getters and display them on screen
	 */
	public void userInformation() {
	}

	/**
	 * Handles the event for the Artarawe button.
	 */
	private void handleArtataweButton() {
		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("artatawe.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) artataweButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Handles the event for the defaut button and allows you to choose a default image.
	 */
	private void handleDefaultButton() {
		ChooseDefault cd = new ChooseDefault();
		LogInPageController.u.setImagePath(cd.chooseImage());
		showProfileImage();
	}

	/**
	 * Handles the event for a favourite user.
	 */
	private void handleFavUsersButton() {
		try {
			Parent favPage = FXMLLoader.load(getClass().getResource("favourites.fxml"));

			Scene favPageScene = new Scene(favPage);

			Stage newStage = (Stage) favUsersButton.getScene().getWindow();
			newStage.setScene(favPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Handles the event for won artwork.
	 */
	private void handleWonArtworksButton() {
		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("history.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) wonArtworksButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Handles the event for the sold artwork button.
	 */
	private void handleSoldArtworksButton() {
		try {
			Parent soldPage = FXMLLoader.load(getClass().getResource("sold.fxml"));

			Scene soldPageScene = new Scene(soldPage);

			Stage newStage = (Stage) soldArtworksButton.getScene().getWindow();
			newStage.setScene(soldPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Handles the draw imagine event, and opens the DrawImage window
	 */
	private void handleDrawImageButton() {
		DrawImage d = new DrawImage();
		Parent editRoot = new BorderPane(d.buildGUI());
		editRoot.setVisible(true);

		Scene editScene = new Scene(editRoot, d.getWindowWidth(), d.getWindowHeight());

		Stage editStage = new Stage();
		editStage.setScene(editScene);
		editStage.initModality(Modality.APPLICATION_MODAL);
		editStage.showAndWait();
	}

	/**
	 * Show the profile image on the screen.
	 */
	private void showProfileImage() {
		try {
			Image image = new Image(LogInPageController.u.getImagePath());
			profileImage.setImage(image);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 *  Handles the new artwork button event.
	 */
	private void handleNewArtworkButton() {
		try {
			Parent newArtworkPage = FXMLLoader.load(getClass().getResource("newArtwork.fxml"));

			Scene newArtworkPageScene = new Scene(newArtworkPage);

			Stage newStage = (Stage) newArtworkButton.getScene().getWindow();
			newStage.setScene(newArtworkPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Handles the event for the edit profile button.
	 */
	private void handleEditProfileButton() {
		try {
			Parent editPage = FXMLLoader.load(getClass().getResource("editProfile.fxml"));

			Scene editPageScene = new Scene(editPage);

			Stage newStage = (Stage) editProfileButton.getScene().getWindow();
			newStage.setScene(editPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Handles the event for the log out button.
	 */
	private void handleLogOutButton() {
		try {
			Parent logInPage = FXMLLoader.load(getClass().getResource("logIn.fxml"));

			Scene logInPageScene = new Scene(logInPage);

			Stage newStage = (Stage) logOutButton.getScene().getWindow();
			newStage.setScene(logInPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Show the user's information.
	 */
	private void displayUserInfo() {
		infoArea.setText(LogInPageController.u.getUserInfo());
	}
}
