
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller for the bid history page.
 * @author Anthony Fanias
 *
 */
public class BidHistoryController {

	@FXML
	Button userLoggedIn;
	@FXML
	Button artataweButton;
	@FXML
	Label displayUserName;
	@FXML
	TextArea wonArtworks;
	@FXML
	TextArea bidOn;

	BidHistory b = new BidHistory();

	/**
	 * Initialize the event handlers.
	 */
	public void initialize() {

		userLoggedIn.setOnAction(e -> {
			handleUserLoggedIn();
		});

		artataweButton.setOnAction(e -> {
			handleArtataweButton();
		});

		wonArtworks.setText(b.wonArtworks(LogInPageController.u.getUserName()));
		bidOn.setText(b.buyerBids(LogInPageController.u.getUserName()));

		displayUserName.setText(LogInPageController.u.getUserName());
	}

	/**
	 * User logged in event handler.
	 */
	private void handleUserLoggedIn() {
		try {
			Parent profilePage = FXMLLoader.load(getClass().getResource("profile.fxml"));

			Scene profilePageScene = new Scene(profilePage);

			Stage newStage = (Stage) userLoggedIn.getScene().getWindow();
			newStage.setScene(profilePageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Artatawe event handler.
	 */
	private void handleArtataweButton() {
		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("artatawe.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) artataweButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
