
import javafx.scene.image.Image;

/**
 * This Class represents an artwork.
 * @author Josh Jones, Matt Leonard
 *
 */
public class Artworks {
	// Constant variables
	protected String title;
	protected final int itemID;
	protected String sellerName;
	protected String description;
	protected String photoPath;
	protected Image photo;
	protected String artistName;
	protected int yearCreated;
	protected double reservePrice;
	protected int bidLimit;
	protected String date;
	protected String artworkType;

	// Updatable variables
	protected int bidsPlaced;
	protected double currentPrice;
	protected String dateSold;
	 /** Constructs an artwork.
	  * 
	  * @param itemID
	  * @param title
	  * @param photoPath
	  * @param artistName
	  * @param yearCreated
	  * @param reservePrice
	  * @param bidLimit
	  * @param date
	  * @param bidsPlaced
	  * @param currentPrice
	  * @param sellerName
	  * @param dateSold
	  * @param description
	  */
	public Artworks(int itemID, String title, String photoPath, String artistName, int yearCreated, double reservePrice,
			int bidLimit, String date, int bidsPlaced, double currentPrice, String sellerName, String dateSold,
			String description) {

		this.title = title;
		this.itemID = itemID;
		this.description = description;
		this.artistName = artistName;
		this.yearCreated = yearCreated;
		this.reservePrice = reservePrice;
		this.bidLimit = bidLimit;
		this.date = date;
		this.sellerName = sellerName;
		setCurrentPrice(currentPrice);
		setBidsPlaced(bidsPlaced);
		setDateSold(dateSold);
		this.photoPath = photoPath;
		// setPhotoPath(photoPath);
	}
	/**Constructs an artwork.
	 * 
	 * @param itemID
	 * @param title
	 * @param photoPath
	 * @param artistName
	 * @param yearCreated
	 * @param reservePrice
	 * @param bidLimit
	 * @param date
	 * @param bidsPlaced
	 * @param currentPrice
	 * @param sellerName
	 * @param dateSold
	 */
	public Artworks(int itemID, String title, String photoPath, String artistName, int yearCreated, double reservePrice,
			int bidLimit, String date, int bidsPlaced, double currentPrice, String sellerName, String dateSold) {

		this.title = title;
		this.itemID = itemID;
		this.artistName = artistName;
		this.yearCreated = yearCreated;
		this.reservePrice = reservePrice;
		this.bidLimit = bidLimit;
		this.date = date;
		this.sellerName = sellerName;
		setCurrentPrice(currentPrice);
		setBidsPlaced(bidsPlaced);
		setDateSold(dateSold);
		this.photoPath = photoPath;
		// setPhotoPath(photoPath);
	}
	
	/** Constructor for creating a new Artwork.
	 * 
	 * @param itemID
	 * @param title
	 * @param photoPath
	 * @param artistName
	 * @param yearCreated
	 * @param reservePrice
	 * @param bidLimit
	 * @param date
	 * @param sellerName
	 * @param description
	 */
	public Artworks(int itemID, String title, String photoPath, String artistName, int yearCreated, double reservePrice,
			int bidLimit, String date, String sellerName, String description) {
		this.title = title;
		this.itemID = itemID;
		this.artistName = artistName;
		this.yearCreated = yearCreated;
		this.reservePrice = reservePrice;
		this.bidLimit = bidLimit;
		this.date = date;
		this.sellerName = sellerName;
		this.photoPath = photoPath;
	}

	/**
	 * Returns number of bids placed.
	 * @return number of bids placed.
	 */
	public int getBidsPlaced() {
		return bidsPlaced;
	}

	/**
	 * Sets the number of bids placed
	 * @param bidsPlaced
	 */
	public void setBidsPlaced(int bidsPlaced) {
		this.bidsPlaced = bidsPlaced;
	}

	/**
	 *  Returns the current price of the artwork.
	 * @return the current price of the artwork.
	 */
	public double getCurrentPrice() {
		return currentPrice;
	}

	/** Sets the current price of the artwork.
	 * 
	 * @param currentPrice
	 */
	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	/**
	 *  Returns the title of the artwork.
	 * @return the title of the artwork.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 *  Returns the ItemID of the Artwork.
	 * @return Returns the ItemID of the Artwork.
	 */
	public int getItemID() {
		return itemID;
	}

	/**
	 *  Returns the description of the artwork.
	 * @return the description of the artwork
	 */
	public String getDescription() {
		return description;
	}

	/**
	 *  Returns the photo of the artwork
	 * @return the photo of the artwork
	 */
	public Image getPhoto() {
		return photo;
	}

	/**
	 * Returns the artists name of the artwork.
	 * @return the artists name of the artwork.
	 */
	public String getArtistName() {
		return artistName;
	}

	/**
	 *  Returns the year the artwork was created.
	 * @return the year the artwork was created.
	 */
	public int getYearCreated() {
		return yearCreated;
	}

	/**
	 * Returns the reserved price of the artwork.
	 * @return the reserved price of the artwork.
	 */
	public double getReservePrice() {
		return reservePrice;
	}

	/**
	 *  Return the bid limit of the artwork.
	 * @return the bid limit of the artwork.
	 */
	public int getBidLimit() {
		return bidLimit;
	}

	/**
	 * Return the date the artwork was put up for auction.
	 * @return the date the artwork was put up for auction.
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Return the date the artwork was sold.
	 * @return the date the artwork was sold.
	 */
	public String getDateSold() {
		return dateSold;
	}

	/**
	 *  Sets the date the artwork was sold.
	 * @param dateSold
	 */
	public void setDateSold(String dateSold) {
		this.dateSold = dateSold;
	}

	/**
	 * Return the photo path of the artwork.
	 * @return the photo path of the artwork.
	 */
	public String getPhotoPath() {
		return photoPath;
	}

	/**
	 * Return the seller name of the artwork.
	 * @return the seller name of the artwork.
	 */
	public String getSellerName() {
		return sellerName;
	}

	/**
	 *  Sets the photo path for the artwork.
	 * @param photoPath
	 */
	public void setPhotoPath(String photoPath) {
		String result = "file:///" + photoPath;
		this.photoPath = result;
	}

	/**
	 *  Returns the type of artwork, either sculpture or painting.
	 * @return the type of artwork.
	 */
	public String getType() {
		return artworkType;
	}

	/**
	 * All the information.
	 * @return the result of the information.
	 */
	public String toString() {
		String result = "\n" + this.artworkType + "," + this.itemID + "," + this.title + "," + this.photoPath + ","
				+ this.artistName + "," + this.yearCreated + "," + this.reservePrice + "," + this.bidLimit + ","
				+ this.date + "," + this.bidsPlaced + "," + this.currentPrice + "," + this.sellerName + ","
				+ this.dateSold;
		if (this.description != null) {
			result += "," + this.description;
		}
		return result;
	}

	
/*	public String getInfo() {
		return "Title: " + this.title + "\n" +
				"Name of Artist: " + this.artistName + "\n" +
				"Year Created: " + this.yearCreated + "\n" +
				"Reserve Price: " + this.reservePrice + "\n" +
				"Date: " + this.date + "\n" +
				"Description: " + this.description + "\n" +
				"Name of Seller: " + this.sellerName + "\n";
	}*/
}