import javafx.embed.swing.SwingFXUtils;
import javafx.stage.FileChooser;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * This class allows the user to browse for images.
 * 
 * @author Abdi Rahmaan, Matthew Payne
 *
 */
public class ChooseDefault {

	ImageView userView;
	String imagePath;

	/**
	 * Create Choose Default object.
	 */
	public ChooseDefault() {

	}

	/**
	 * Choose an image to use.
	 * 
	 * @return File path of the image.
	 */
	public String chooseImage() {
		final FileChooser fileChooser = new FileChooser();

		/*
		 * FileChooser - this browses the file system and loads wanted files.
		 * ExtensionFilter class - defines the file types we want: JPEG or PNG.
		 */
		FileChooser.ExtensionFilter justJPG = new FileChooser.ExtensionFilter("JPG files (*.JPG)", "*.JPG", "*.jpg");
		FileChooser.ExtensionFilter justPNG = new FileChooser.ExtensionFilter("PNG files (*.PNG)", "*.PNG", "*.png");
		fileChooser.getExtensionFilters().addAll(justJPG, justPNG);

		/*
		 * File object created - This defines the file in the file system a user may
		 * choose. OR a null - this means a valid file may not have been chosen. why-
		 * user may have clicked cancel.
		 */
		File file = fileChooser.showOpenDialog(null);

		try {
			BufferedImage bufferedImage = ImageIO.read(file);
			Image image = SwingFXUtils.toFXImage(bufferedImage, null);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return ("file:///" + file.getAbsolutePath());
	}

}