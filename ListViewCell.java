import javafx.scene.control.ListCell;

/**
 * 
 * @author 
 *
 */
public class ListViewCell extends ListCell<Artworks> {
	@Override
	/**
	 * 
	 */
	protected void updateItem(Artworks item, boolean empty) {
		super.updateItem(item, empty);

		if (empty || item == null) {
			setText(null);
			setGraphic(null);
		} else {
			Information info = new Information();
			info.setInfo(item);
			setGraphic(info.getBox());
		}
	}

}
