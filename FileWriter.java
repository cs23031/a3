import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * This class writes to the .txt files.
 * @author Jack Laidlaw
 *
 */
public class FileWriter {

	/**
	 * Writing User object information to a file
	 * 
	 * @param info
	 */
	public void writeUserInfo(User info) {
		File userFile = new File("UserInfo.txt");
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileOutputStream(userFile, true));
		} catch (FileNotFoundException e) {
			System.out.println("Cannot open file");
			System.exit(0);
		}
		out.println(info);
		out.flush();
		out.close();
	}

	/**
	 * Writing User object information to a file
	 * 
	 * @param info
	 */
	public void writeFavUserInfo(User info) {
		File favUserFile = new File("FavUserInfo.txt");
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileOutputStream(favUserFile, true));
		} catch (FileNotFoundException e) {
			System.out.println("Cannot open file");
			System.exit(0);
		}
		ArrayList<FavUser> favUsers = info.getFavUsers();
		String result = info.getUserName();
		for (int i = 0; i < favUsers.size(); i++){
			result += "," + favUsers.get(i);
		}
		out.println(result);
		out.flush();
		out.close();
	}
	
	public void updateFavUsers(User info) throws IOException {
		
		BufferedReader file = new BufferedReader(new FileReader("FavUserInfo.txt"));
        String line;
        StringBuffer inputBuffer = new StringBuffer();
        
        while ((line = file.readLine()) != null && ! line.startsWith(info.getUserName())) {
            inputBuffer.append(line);
            inputBuffer.append("\r\n");
        }
        ArrayList<FavUser> favUsers = info.getFavUsers();
		String result = info.getUserName();
		for (int i = 0; i < favUsers.size(); i++){
			result += "," + favUsers.get(i);
		}
        inputBuffer.append(result);
        inputBuffer.append("\r\n");
        while ((line = file.readLine()) != null) {
            inputBuffer.append(line);
            inputBuffer.append("\r\n");
        }
        String inputStr = inputBuffer.toString();
        file.close();
        
        PrintWriter fileOut = new PrintWriter(new FileOutputStream("FavUserInfo.txt"));
        fileOut.println(inputStr);
        fileOut.close();
	}
	
	/**
	 * Writing Artwork object information to a file
	 * 
	 * @param info
	 */
	public void writeArtworkInfo(Artworks info) {
		File artworkFile = new File("ArtworkInfo.txt");
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileOutputStream(artworkFile, true));
		} catch (FileNotFoundException e) {
			System.out.println("Cannot open file");
			System.exit(0);
		}
		out.println(info);
		out.flush();
		out.close();
	}

	/**
	 * Writing BidHistory information to a file
	 * 
	 * @param info
	 * @throws IOException
	 */
	public void writeBidding(String info, int ID) throws IOException {

		BufferedReader file = new BufferedReader(new FileReader("biddingInfo.txt"));
		String line;
		StringBuffer inputBuffer = new StringBuffer();

		while ((line = file.readLine()) != null && !line.startsWith(Integer.toString(ID))) {
			inputBuffer.append(line);
			inputBuffer.append("\n");
		}
		inputBuffer.append(info);
		inputBuffer.append("\n");
		while ((line = file.readLine()) != null) {
			inputBuffer.append(line);
			inputBuffer.append("\n");
		}
		String inputStr = inputBuffer.toString();
		file.close();

		PrintWriter fileOut = new PrintWriter(new FileOutputStream("biddingInfo.txt"));
		fileOut.println(inputStr);
		fileOut.close();

	}

	/**
	 * Write the bid history.
	 * @param info
	 */
	public void writeBidHistory(String info) {
		File BidHistoryFile = new File("BidHistoryInfo.txt");
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileOutputStream(BidHistoryFile, true));
		} catch (FileNotFoundException e) {
			System.out.println("Cannot open file");
			System.exit(0);
		}
		out.println(info);
		out.flush();
		out.close();
	}

	/**
	 * Update the artwork file.
	 * @param info
	 * @param ID
	 * @throws IOException
	 */
	public void updateArtwork(String info, int ID) throws IOException {

		BufferedReader file = new BufferedReader(new FileReader("ArtworkInfo.txt"));
		String line;
		StringBuffer inputBuffer = new StringBuffer();

		while ((line = file.readLine()) != null && !line.startsWith(Integer.toString(ID))) {
			inputBuffer.append(line);
			inputBuffer.append("\r\n");
		}
		inputBuffer.append(info);
		inputBuffer.append("\r\n");
		while ((line = file.readLine()) != null) {
			inputBuffer.append(line);
			inputBuffer.append("\r\n");
		}
		String inputStr = inputBuffer.toString();
		file.close();

		PrintWriter fileOut = new PrintWriter(new FileOutputStream("ArtworkInfo.txt"));
		fileOut.println(inputStr);
		fileOut.close();
	}
}
