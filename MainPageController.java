import java.io.IOException;
import java.util.ArrayList;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ListView;
import javafx.scene.control.ListCell;

/**
 * The controller for the artatawe.fxml where it displays the artworks, a filter
 * for the artworks and user button.
 * 
 * @authors Anthony Fanias, Jack Laidlaw.
 */
public class MainPageController {

	@FXML
	Button userLoggedIn;
	@FXML
	Label displayUserName;
	@FXML
	ChoiceBox<String> filterArtwork;
	@FXML
	ListView<Artworks> artworkList;

	Browsing q = new Browsing();

	public static Artworks a;
	public static Sculpture s;
	public static Painting p;

	/**
	 * Initializes the event handeler.
	 */
	public void initialize() {

		userLoggedIn.setOnAction(e -> {
			// make it go to the Profile page
			handleUserLoggedIn();
		});

		displayUserName.setText(LogInPageController.u.getUserName());

		ObservableList<Artworks> items = FXCollections.observableArrayList(q.displayArtworks());
		artworkList.setItems(items);
		artworkList.setCellFactory(new Callback<ListView<Artworks>, javafx.scene.control.ListCell<Artworks>>() {
			@Override
			
			/**
			 * Formats the cells in the lists.
			 */
			public ListCell<Artworks> call(ListView<Artworks> artworkList) {
				return new ListViewCell();
			}
		});
		artworkList.setOnMouseClicked(e -> {
			handleSelectedItem();
		});

		filterArtwork.setOnAction(e -> {
			handleSwitching();
		});

	}

	/**
	 * Handles the users logged in event. 
	 */
	private void handleUserLoggedIn() {
		try {
			Parent profilePage = FXMLLoader.load(getClass().getResource("profile.fxml"));

			Scene profilePageScene = new Scene(profilePage);

			Stage newStage = (Stage) userLoggedIn.getScene().getWindow();
			newStage.setScene(profilePageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Handles the selected item event. 
	 */
	private void handleSelectedItem() {
		 a = artworkList.getSelectionModel().getSelectedItem();
	
		if (a.getType().equals("S")) {
			s = (Sculpture) a; 
		} else {
			p = (Painting) a;
		}
		
		
		try {
			Parent viewPage = FXMLLoader.load(getClass().getResource("viewArtwork.fxml"));

			Scene viewPageScene = new Scene(viewPage);

			Stage newStage = (Stage) userLoggedIn.getScene().getWindow();
			newStage.setScene(viewPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Handles the switching event. 
	 */
	private void handleSwitching() {
		ObservableList<Artworks> items;

		switch (filterArtwork.getValue()) {
		case "All":
			artworkList.getItems().clear();
			items = FXCollections.observableArrayList(q.displayArtworks());
			artworkList.setItems(items);
			break;
		case "Sculptures":
			artworkList.getItems().clear();
			items = FXCollections.observableArrayList(q.getAllSculptures());
			artworkList.setItems(items);
			break;
		case "Paintings":
			artworkList.getItems().clear();
			items = FXCollections.observableArrayList(q.getAllPaintings());
			artworkList.setItems(items);
			break;
		}
	}

}
