import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * This class reads from the .txt files.
 * @author Jack Laidlaw
 *
 */
public class FileReader {

	/**
	 * Given a Username this method will return that user object or null if there is
	 * no username equal to the one given
	 * 
	 * @param thisUser
	 * @return User or null
	 */
	public User getUserName(String thisUser) {
		File fileIn = new File("UserInfo.txt");
		Scanner in = null;
		try {
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e) {
			System.out.println("No file");
		}
		return readUser(in, thisUser);
	}

	/**
	 * Given a Username this method will return that user object or null if there is
	 * no username equal to the one given.
	 * 
	 * @param in
	 * @param thisUser
	 * @return
	 */
	private User readUser(Scanner in, String thisUser) {
		while (in.hasNextLine()) {
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			String Username = line.next();
			if (Username.equals(thisUser)) {
				String name = line.next();
				String address = line.next();
				String phoneNumber = line.next();
				String imagePath = line.next();
				User u = new User(thisUser, name, address, phoneNumber, imagePath);
				line.close();
				return u;
			}
			line.close();
		}
		return null;
	}
	
	/**
	 * Get all the users.
	 * @param thisUser
	 * @return All the users.
	 */
	public Queue<User> getAllUsers(String thisUser){
		File fileIn = new File("UserInfo.txt");
		Scanner in = null;
		try{
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e){
			System.out.println("No file");
		}
		return readAllUsers(in,thisUser);
	}
	/**
	 * Read all users.
	 * @param in
	 * @param thisUser
	 * @return All the users.
	 */
	private Queue<User> readAllUsers(Scanner in,String thisUser){
		Queue<User> q = new LinkedList<>();
		while(in.hasNextLine()){
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			String Username = line.next();
			if(!Username.equals(thisUser)){
				String name = line.next();
				String address = line.next();
				String phoneNumber = line.next();
				String imagePath = line.next();
				User u = new User(Username,name,address,phoneNumber,imagePath);
				q.add(u);
			}
		}
		return q;
	}
	
	/**
	 * Given an Artwork ID this method will return that artwork object or null if
	 * there is no ID equal to the one given
	 * 
	 * @param ID
	 * @return Artworks or null
	 */
	public Artworks getArtwork(int ID) {
		File fileIn = new File("ArtworkInfo.txt");
		Scanner in = null;
		try {
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e) {
			System.out.println("No file");
		}
		return readArtwork(in, ID);
	}

	/**
	 * Read in an artwork from the file.
	 * 
	 * @param in
	 * @param ID
	 * @return the artwork.
	 */
	private Artworks readArtwork(Scanner in, int ID) {
		while (in.hasNextLine()) {
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			String artType = line.next();
			switch (artType) {
			case ("S"):
				Sculpture s = readSculpture(line);
				if (s.getItemID() == ID) {
					return s;
				}
				break;
			case ("P"):
				Painting p = readPainting(line);
				if (p.getItemID() == ID) {
					return p;
				}
				break;
			default:
				System.out.println("Can't read artwork");
				System.exit(0);

			}
			line.close();
		}
		return null;
	}

	/**
	 * This method builds sculpture objects from file.
	 * @param line
	 * @return the sculpture.
	 */
	private Sculpture readSculpture(Scanner line) {
		int itemID = line.nextInt();
		String title = line.next();
		String photoPath = line.next();
		String name = line.next();
		int year = line.nextInt();
		double reservePrice = line.nextDouble();
		int bidLimit = line.nextInt();
		String dateTime = line.next();
		int bidsPlaced = line.nextInt();
		double currentPrice = line.nextDouble();
		String sellerName = line.next();
		String dateSold = line.next();
		if (line.hasNextInt()) {
			int dW = line.nextInt();
			int dH = line.nextInt();
			int dD = line.nextInt();
			String material = line.next();
			if (line.hasNext()) {
				String[] additionalPhotos = new String[5];
				int i = 0;
				while (line.hasNext()) {
					additionalPhotos[i] = line.next();
					i++;
				}
				Sculpture s1 = new Sculpture(itemID, title, photoPath, name, year, reservePrice, bidLimit, dateTime,
						bidsPlaced, currentPrice, sellerName, dateSold, dW, dH, dD, material, additionalPhotos);
				return s1;
			}
			Sculpture s2 = new Sculpture(itemID, title, photoPath, name, year, reservePrice, bidLimit, dateTime,
					bidsPlaced, currentPrice, sellerName, dateSold, dW, dH, dD, material);
			return s2;
		} else {
			String description = line.next();
			int dW = line.nextInt();
			int dH = line.nextInt();
			int dD = line.nextInt();
			String material = line.next();
			if (line.hasNext()) {
				String[] additionalPhotos = new String[5];
				int i = 0;
				while (line.hasNext()) {
					additionalPhotos[i] = line.next();
					i++;
				}
				Sculpture s3 = new Sculpture(itemID, title, photoPath, name, year, reservePrice, bidLimit, dateTime,
						bidsPlaced, currentPrice, sellerName, dateSold, description, dW, dH, dD, material,
						additionalPhotos);
				return s3;
			}
			Sculpture s4 = new Sculpture(itemID, title, photoPath, name, year, reservePrice, bidLimit, dateTime,
					bidsPlaced, currentPrice, sellerName, dateSold, description, dW, dH, dD, material);
			return s4;
		}

	}

	/**
	 * builds painting objects from file.
	 * @param line
	 * @return the painting.
	 */
	private Painting readPainting(Scanner line) {
		int itemID = line.nextInt();
		String title = line.next();
		String photoPath = line.next();
		String name = line.next();
		int year = line.nextInt();
		double reservePrice = line.nextDouble();
		int bidLimit = line.nextInt();
		String dateTime = line.next();
		int bidsPlaced = line.nextInt();
		double currentPrice = line.nextDouble();
		String sellerName = line.next();
		String dateSold = line.next();
		if (line.hasNextInt()) {
			int dW = line.nextInt();
			int dH = line.nextInt();
			Painting pt = new Painting(itemID, title, photoPath, name, year, reservePrice, bidLimit, dateTime,
					bidsPlaced, currentPrice, sellerName, dateSold, dW, dH);
			return pt;
		} else {
			String description = line.next();
			int dW = line.nextInt();
			int dH = line.nextInt();
			Painting p = new Painting(itemID, title, photoPath, name, year, reservePrice, bidLimit, dateTime,
					bidsPlaced, currentPrice, sellerName, dateSold, description, dW, dH);
			return p;
		}
	}

	/**
	 * This method will return all artworks in the artworks file
	 * 
	 * @return All artworks
	 */
	public Queue<Artworks> getAllArtworks() {
		File fileIn = new File("ArtworkInfo.txt");
		Scanner in = null;
		try {
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e) {
			System.out.println("No file");
		}
		return readAllArtworks(in);
	}

	/**
	 * Builds all the artworks and puts them in the queue
	 * @param in
	 * @return queue of all artworks
	 */
	private Queue<Artworks> readAllArtworks(Scanner in) {
		Queue<Artworks> allArtworks = new LinkedList<>();
		while (in.hasNextLine()) {
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			String artType = line.next();
			switch (artType) {
			case ("S"):
				Sculpture s = readSculpture(line);
				allArtworks.add(s);
				break;
			case ("P"):
				Painting p = readPainting(line);
				allArtworks.add(p);
				break;
			default:
				System.out.println("Can't read artwork");
				System.exit(0);
			}

		}
		return allArtworks;
	}

	/**
	 * Login page
	 * 
	 * @param userName
	 * @return Boolean if username is in userInfo file
	 */
	public Boolean login(String userName) {
		if (getUserName(userName) != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 *  Reads bids from the file.
	 * @param itemID
	 * @return the bidding object.
	 */
	public Bidding readBidding(int itemID) {
		File fileIn = new File("BiddingInfo.txt");
		Scanner in = null;
		try {
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e) {
			System.out.println("No file");
		}
		return readBiddingFile(in, itemID);
	}

	/**
	 *  Reads bids from the file.
	 * @param in
	 * @param itemID
	 * @return the bidding object.
	 */
	private Bidding readBiddingFile(Scanner in, int itemID) {
		while (in.hasNextLine()) {
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			int ID = line.nextInt();
			if (ID == itemID) {
				double bidPlaced = line.nextDouble();
				String user = line.next();
				String sellerName = line.next();
				String date = line.next();
				int counter = line.nextInt();
				Bidding b = new Bidding(bidPlaced, counter, ID, user, sellerName, date);
				line.close();
				return b;
			}
		}
		return null;
	}

	/**
	 *  Read the won artworks.
	 * @param user
	 * @return The won artworks.
	 */
	public ArrayList<Integer> readWonHistory(String user) {
		File fileIn = new File("BiddingInfo.txt");
		Scanner in = null;
		try {
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e) {
			System.out.println("No file");
		}
		return readWon(in, user);
	}

	/**
	 * Read the won artworks.
	 * @param in
	 * @param user
	 * @return The won artworks.
	 */
	private ArrayList<Integer> readWon(Scanner in, String user) {
		ArrayList<Integer> ids = new ArrayList<>();
		while (in.hasNextLine()) {
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			int ID = line.nextInt();
			line.nextDouble();
			// GETS THE USER NAME
			String u = line.next();
			line.next();
			line.next();
			int c = line.nextInt();
			// CHECKS THE ITEM IS SOLD
			if ((u.equals(user)) && (c == getArtwork(ID).bidLimit)) {
				ids.add(ID);
			}
		}
		return ids;
	}

	/**
	 * Reads the sold artworks.
	 * @param user
	 * @return The sold artworks.
	 */
	public ArrayList<Integer> readSoldHistory(String user) {
		File fileIn = new File("BiddingInfo.txt");
		Scanner in = null;
		try {
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e) {
			System.out.println("No file");
		}
		return readSold(in, user);
	}

	/**
	 * Reads the sold artworks.
	 * @param in
	 * @param user
	 * @return The sold artworks.
	 */
	private ArrayList<Integer> readSold(Scanner in, String user) {
		ArrayList<Integer> ids = new ArrayList<>();
		while (in.hasNextLine()) {
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			int ID = line.nextInt();
			line.nextDouble();
			line.next();
			// GETS THE SELLER NAME
			String s = line.next();
			line.next();
			int c = line.nextInt();
			// CHECKS THE ITEM IS SOLD
			if ((s.equals(user)) && (c == getArtwork(ID).bidLimit)) {
				ids.add(ID);
			}
		}
		return ids;
	}

	/**
	 * Reads the current artworks.
	 * @param user
	 * @return The curent artworks.
	 */
	public ArrayList<Integer> readCurrentHistory(String user) {
		File fileIn = new File("BiddingInfo.txt");
		Scanner in = null;
		try {
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e) {
			System.out.println("No file");
		}
		return readCurrent(in, user);
	}

	/**
	 * Reads the current artworks.
	 * @param in
	 * @param user
	 * @return The curent artworks.
	 */
	private ArrayList<Integer> readCurrent(Scanner in, String user) {
		ArrayList<Integer> ids = new ArrayList<>();
		while (in.hasNextLine()) {
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			int ID = line.nextInt();
			line.nextDouble();
			line.next();
			String s = line.next();
			line.next();
			int c = line.nextInt();
			if (s.equals(user) && (c < getArtwork(ID).bidLimit)) {
				ids.add(ID);
			}
		}
		return ids;
	}

	/**
	 * Read the bids placed.
	 * @param user
	 * @return The bids placed.
	 */
	public ArrayList<Integer> readBidsPlaced(String user) {
		File fileIn = new File("BidHistoryInfo.txt");
		Scanner in = null;
		try {
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e) {
			System.out.println("No file");
		}
		return readBids(in, user);
	}

	/**
	 * Read the bids placed.
	 * @param in
	 * @param user
	 * @return The bids placed.
	 */
	private ArrayList<Integer> readBids(Scanner in, String user) {
		ArrayList<Integer> ids = new ArrayList<>();
		while (in.hasNextLine()) {
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			int ID = line.nextInt();
			line.nextDouble();
			String u = line.next();
			line.next();
			line.next();
			// CHECKS THE ID HASNT ALREADY BEEN ENTERED
			if ((u.equals(user)) && (!ids.contains(ID))) {
				ids.add(ID);
			}
		}
		return ids;
	}

	/**
	 * Read the bid time.
	 * @param itemID
	 * @return The bid time.
	 */
	public String outBidTime(int itemID) {
		File fileIn = new File("BidHistoryInfo.txt");
		Scanner in = null;
		try {
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e) {
			System.out.println("No file");
		}
		String s = "";
		while (in.hasNextLine()) {
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			int ID = line.nextInt();
			double bid = line.nextDouble();
			line.next();
			line.next();
			String date = line.next();
			if (ID == itemID) {
				s = s + (bid + " Placed at: " + date + "\n");
			}
		}
		return s;
	}
	public ArrayList<FavUser> readFavUserFile(String username){
		File fileIn = new File("FavUserInfo.txt");
		Scanner in = null;
		try{
			in = new Scanner(fileIn);
		} catch (FileNotFoundException e){
			System.out.println("No file");
		}
		return readFavUser(in, username);	
	}
	
	private ArrayList<FavUser> readFavUser(Scanner in, String username){
		ArrayList<FavUser> fU = new ArrayList<>();
		while(in.hasNextLine()){
			String getLine = in.nextLine();
			Scanner line = new Scanner(getLine);
			line.useDelimiter(",");
			String uName = line.next();
			if(uName.equals(username)){
				while(line.hasNext()){
					String name = line.next();
					String imagePath = line.next();
					FavUser f = new FavUser(name,imagePath);
					fU.add(f);
				}
			}
			line.close();
		}
		return fU;

	}
}
