import javafx.scene.control.ListCell;

/**
 * 
 * @author 
 *
 */
public class ListViewCellU extends ListCell<User> {
	@Override
	/**
	 * 
	 */
	protected void updateItem(User item, boolean empty) {
		super.updateItem(item, empty);

		if (empty || item == null) {
			setText(null);
			setGraphic(null);
		} else {
			Information info = new Information();
			info.setInfo(item);
			setGraphic(info.getBox());
		}
	}
}
