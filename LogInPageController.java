import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * The controller for the logIn.fxml, where the user can log in or register.
 * 
 * @author Anthony Fanias, Jack Laidlaw.
 *
 */
public class LogInPageController {

	@FXML
	Button artataweButton;
	@FXML
	Button logInButton;
	@FXML
	Button newUser;
	@FXML
	TextField enterUsername;
	
	public static User u;

	/**
	 * Initialize the event handlers.
	 */
	public void initialize() {
		logInButton.setOnAction(e -> {
			handleLogInButtonAction();
		});
		
		newUser.setOnAction(e -> {
			handleRegisterButton();
		});
	}

	/**
	 * Change the scene to the users profile.
	 */
	private void changeScene() {

		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("artatawe.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) logInButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Show the error box.
	 */
	public void errorHandler() {
		// error message/box
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setHeaderText("WHOOPSY!");
		alert.setContentText("This user is not registered, please register or enter a valid username.");
		alert.showAndWait();

	}

	/**
	 * Log in event handler.
	 */
	private void handleLogInButtonAction() {
		FileReader x = new FileReader();
		// check if the username is valid and change to the main page scene.
		if (x.login(enterUsername.getText())) {
			this.u = x.getUserName(enterUsername.getText());
			changeScene();
		} else {
			// error message box
			errorHandler();
		}
	}
	
	/**
	 * 
	 */
	private void handleRegisterButton() {
		try {
			Parent newPage = FXMLLoader.load(getClass().getResource("registertest.fxml"));

			Scene newPageScene = new Scene(newPage);

			Stage newStage = (Stage) newUser.getScene().getWindow();
			newStage.setScene(newPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

