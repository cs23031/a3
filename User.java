
import java.io.IOException;
import java.util.ArrayList;

import javafx.scene.image.WritableImage;

/**
 * Class to create a user.
 * 
 * @author Abdi, Matthew Payne
 *
 */
public class User {

	private String userName;
	private String fullName;
	private String telephoneNumber;
	private String address;
	private WritableImage profileImage;
	private ArrayList<FavUser> favUsers;
	private String imagePath;

	/**
	 * Create a user object.
	 * 
	 * @param userName
	 * @param fullname
	 * @param telephoneNumber
	 * @param address
	 */
	public User(String userName, String fullName, String address, String telephoneNumber) {
		this.userName = userName;
		this.fullName = fullName;
		this.address = address;
		this.telephoneNumber = telephoneNumber;
		this.favUsers = new ArrayList<FavUser>();
	}

	/**
	 * Create a user object with a profile image
	 * 
	 * @param userName
	 * @param fullName
	 * @param address
	 * @param telephoneNumber
	 * @param imagePath
	 */
	public User(String userName, String fullName, String address, String telephoneNumber, String imagePath) {
		this.userName = userName;
		this.fullName = fullName;
		this.address = address;
		this.telephoneNumber = telephoneNumber;
		this.imagePath = "file:///" + imagePath;
		this.favUsers = new ArrayList<FavUser>();
	}	

	/**
	 * Constructor for sub class.
	 */
	protected User(String username, String imagePath) {
		this.userName = username;
		this.imagePath = imagePath;
	}

	/**
	 * toString method.
	 */
	public String toString() {
		return "\n" + this.userName + " , " + this.fullName + " , " + this.address + " , " + this.telephoneNumber + " , "
				+ this.imagePath;
	}

	/**
	 * Get the user name.
	 * 
	 * @return The user name.
	 */
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Get the user's full name.
	 * 
	 * @return The user's full name.
	 */
	public String getFullName() {
		return this.fullName;
	}

	/**
	 * Get the user's telephone number.
	 * 
	 * @return The user's telephone number.
	 */
	public String getTelephoneNumber() {
		return this.telephoneNumber;
	}

	/**
	 * Get the user's address.
	 * 
	 * @return The user's address.
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 * Get the user's profile image.
	 * 
	 * @return The user's profile image.
	 */
	public WritableImage getProfileImage() {
		return this.profileImage;
	}

	/**
	 * Get the user's favourite users.
	 * 
	 * @return The user's favourite users.
	 */
	public ArrayList<FavUser> getFavUsers() {
		return this.favUsers;
	}

	/**
	 * Add a new favourite user
	 * 
	 * @param username
	 * @param profileImage
	 * @throws IOException 
	 */
	public void addFavUser(String username, String  imagePath) throws IOException {
		FileWriter w = new FileWriter();
		FavUser f = new FavUser(username, imagePath);
		this.favUsers.add(f);
		w.updateFavUsers(this);
	}
	
	/**
	 * Removes a favourite user
	 * 
	 * @param username
	 * @param imagePath
	 */
	public void removeFavUser(String username, String  imagePath){
		FavUser f = new FavUser(username, imagePath);
		for(int i = 0; i < favUsers.size(); i++){
			if (this.favUsers.get(i).getUserName().equals(f.getUserName())){
				this.favUsers.remove(i);
			}
		}
	}

	/**
	 * Get the profile image path.
	 * 
	 * @return The profile image path.
	 */
	public String getImagePath() {
		return this.imagePath;
	}

	/**
	 * Set the image path.
	 */
	public void setImagePath(String path) {
		this.imagePath = path;
	}

	/**
	 * Set full name.
	 * 
	 * @param fullName
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * Set telephone number.
	 * 
	 * @param telephoneNumber
	 */
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	/**
	 * Set address.
	 * 
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Set favourite users.
	 * 
	 * @param favUsers
	 */
	public void setFavUsers(ArrayList<FavUser> favUsers) {
		this.favUsers = favUsers;
	}

	/**
	 * Set user name.
	 * 
	 * @param text
	 */
	public void setUserName(String text) {
		this.userName = text;
	}

	/**
	 * 
	 * @return
	 */
	public String getUserInfo() {
		return "Username: " + this.userName + " \n" + 
				"Full name: " + this.fullName + "\n" +
				"Address: " + this.address + "\n" + 
				"Telephone number: " + this.telephoneNumber + "\n";
	}

}
