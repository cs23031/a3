import java.util.LinkedList;
import java.util.Queue;

/**
 * This class browses for artworks.
 * @author Jack Laidlaw.
 *
 */
public class Browsing {
	private FileReader r = new FileReader();

	/**
	 * method to get all artworks stored in the system and display them
	 * @return all artworks.
	 */
	public Queue<Artworks> displayArtworks() {
		Queue<Artworks> artworks = new LinkedList<>(r.getAllArtworks());
		return artworks;
	}

	/**
	 * Get all the sculptures.
	 * @return all sculptures.
	 */
	public Queue<Sculpture> getAllSculptures() {
		Queue<Artworks> all = new LinkedList<>(r.getAllArtworks());
		Queue<Sculpture> sculptures = new LinkedList<>();
		while (!all.isEmpty()) {
			Artworks topElement = all.poll();
			if (topElement.getType().equals("S")) {
				sculptures.add((Sculpture) topElement);
			}
		}
		return sculptures;
	}

	/**
	 * Get all the paintings.
	 * @return the paintings.
	 */
	public Queue<Painting> getAllPaintings() {
		Queue<Artworks> all = new LinkedList<>(r.getAllArtworks());
		Queue<Painting> paintings = new LinkedList<>();
		while (!all.isEmpty()) {
			Artworks topElement = all.poll();
			if (topElement.getType().equals("P")) {
				paintings.add((Painting) topElement);
			}
		}
		return paintings;
	}

}
