import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollBar;

/**
 * The controller for creating artworks.
 * 
 * @author Anthony Fanias, Matthew Payne
 *
 */
public class CreatingArtworkController {

	@FXML
	Button userLoggedIn;
	@FXML
	Button artataweButton;
	@FXML
	Button uploadImageButton;
	@FXML
	Button confirmButton;
	@FXML
	ChoiceBox artworkFilter;
	@FXML
	ImageView displayImage;
	@FXML
	TextField artworkName;
	@FXML
	TextField artworkTitle;
	@FXML
	TextField artworkDescription;
	@FXML
	TextField nameOfCreator;
	@FXML
	TextField yearCreated;
	@FXML
	TextField reservePrice;
	@FXML
	TextField bidsAllowed;
	@FXML
	TextField dateTime;
	@FXML
	TextField dimensions;
	@FXML
	TextField mainMaterial;
	@FXML
	Label displayUserName;

	private static String imagePath;
	private static int itemID;
	private FileWriter f = new FileWriter();

	/**
	 * Initialize the event handlers.
	 */
	public void initialize() {

		userLoggedIn.setOnAction(e -> {
			handleUserLoggedIn();
		});

		artataweButton.setOnAction(e -> {
			handleArtataweButton();
		});

		uploadImageButton.setOnAction(e -> {
			handleUploadImageButton();
		});

		confirmButton.setOnAction(e -> {
			makeNewArtwork();
		});

		displayUserName.setText(LogInPageController.u.getUserName());
	}

	/**
	 * The user logged in event handler.
	 */
	private void handleUserLoggedIn() {
		try {
			Parent profilePage = FXMLLoader.load(getClass().getResource("profile.fxml"));

			Scene profilePageScene = new Scene(profilePage);

			Stage newStage = (Stage) userLoggedIn.getScene().getWindow();
			newStage.setScene(profilePageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Artatawe event handler.
	 */
	private void handleArtataweButton() {
		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("artatawe.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) artataweButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Open dialog box to choose an image.
	 */
	private void handleUploadImageButton() {
		ChooseDefault cd = new ChooseDefault();
		imagePath = cd.chooseImage();
		System.out.println(imagePath);
		Image image = new Image(imagePath);
		displayImage.setImage(image);

	}

	/**
	 * Create the new artwork.
	 */
	private void makeNewArtwork() {

		String type = (String) this.artworkFilter.getValue();

		if (type.equalsIgnoreCase("sculpture")) {
			makeSculpture();
		} else {
			makePainting();
		}
		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("profile.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) confirmButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Create the new Sculpture.
	 */
	private void makeSculpture() {
		String title = this.artworkTitle.getText();
		String description = this.artworkDescription.getText();
		String creator = this.nameOfCreator.getText();

		double reserve = Double.parseDouble(this.reservePrice.getText());
		int year = Integer.parseInt(this.yearCreated.getText());
		int allowedBids = Integer.parseInt(this.bidsAllowed.getText());

		String date = this.dateTime.getText();
		String material = this.mainMaterial.getText();
		Scanner in = new Scanner(this.dimensions.getText());
		int dimensionW = in.nextInt();
		int dimensionH = in.nextInt();
		int dimensionD = in.nextInt();
		in.close();

		itemID += 1;
		Sculpture s = new Sculpture(itemID, title, imagePath, creator, year, reserve, allowedBids, date,
				LogInPageController.u.getUserName(), dimensionW, dimensionH, dimensionD, material, description);

		f.writeArtworkInfo(s);

	}

	/**
	 * Create the new Painting.
	 */
	private void makePainting() {
		String title = this.artworkTitle.getText();
		String description = this.artworkDescription.getText();
		String creator = this.nameOfCreator.getText();

		double reserve = Double.parseDouble(this.reservePrice.getText());
		int year = Integer.parseInt(this.yearCreated.getText());
		int allowedBids = Integer.parseInt(this.bidsAllowed.getText());

		String date = this.dateTime.getText();
		String material = this.mainMaterial.getText();
		String image = imagePath;

		Scanner in = new Scanner(this.dimensions.getText());
		int dimensionW = in.nextInt();
		int dimensionH = in.nextInt();
		in.close();

		itemID += 1;
		Painting p = new Painting(itemID, title, image, creator, year, reserve, allowedBids, date, 0, 0.0,
				LogInPageController.u.getUserName(), dimensionW, dimensionH, description);
		f.writeArtworkInfo(p);
	}
}
