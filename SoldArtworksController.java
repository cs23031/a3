import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * 
 * @author payno
 *
 */
public class SoldArtworksController {

	@FXML
	Button userLoggedIn;
	@FXML
	Button artataweButton;
	@FXML
	Label displayUserName;
	@FXML
	TextArea bidOn;
	@FXML
	TextArea sold;

	BidHistory b = new BidHistory();

	/**
	 * 
	 */
	public void initialize() {

		userLoggedIn.setOnAction(e -> {
			handleUserLoggedIn();
		});

		artataweButton.setOnAction(e -> {
			handleArtataweButton();
		});

		sold.setText(b.SoldArtworks(LogInPageController.u.getUserName()));

		displayUserName.setText(LogInPageController.u.getUserName());
	}

	/**
	 * 
	 */
	private void handleUserLoggedIn() {
		try {
			Parent profilePage = FXMLLoader.load(getClass().getResource("profile.fxml"));

			Scene profilePageScene = new Scene(profilePage);

			Stage newStage = (Stage) userLoggedIn.getScene().getWindow();
			newStage.setScene(profilePageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	private void handleArtataweButton() {
		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("artatawe.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) artataweButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
