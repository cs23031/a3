/**
 * This class represented a painting.
 * @author Josh Jones, Matt Leonard
 *
 */
public class Painting extends Artworks {
	private int dimensionW;
	private int dimensionH;

	public Painting(int itemID, String title, String photoPath, String artistName, int yearCreated, double reservePrice,
			int bidLimit, String date, int bidsPlaced, double currentPrice, String sellerName, String dateSold,
			String description, int dimensionW, int dimensionH) {

		super(itemID, title, photoPath, artistName, yearCreated, reservePrice, bidLimit, date, bidsPlaced, currentPrice,
				sellerName, dateSold, description);
		this.dimensionW = dimensionW;
		this.dimensionH = dimensionH;
		this.artworkType = "P";

	}

	public Painting(int itemID, String title, String photoPath, String artistName, int yearCreated, double reservePrice,
			int bidLimit, String date, int bidsPlaced, double currentPrice, String sellerName, String dateSold,
			int dimensionW, int dimensionH) {

		super(itemID, title, photoPath, artistName, yearCreated, reservePrice, bidLimit, date, bidsPlaced, currentPrice,
				sellerName, dateSold);
		this.dimensionW = dimensionW;
		this.dimensionH = dimensionH;
		this.artworkType = "P";

	}

	/**
	 * Constructor to add new painting.
	 * 
	 * @param itemID
	 * @param title
	 * @param photoPath
	 * @param artistName
	 * @param yearCreated
	 * @param reservePrice
	 * @param bidLimit
	 * @param date
	 * @param bidsPlaced
	 * @param currentPrice
	 * @param sellerName
	 * @param dimensionW
	 * @param dimensionH
	 */
	public Painting(int itemID, String title, String photoPath, String artistName, int yearCreated, double reservePrice,
			int bidLimit, String date, int bidsPlaced, double currentPrice, String sellerName, int dimensionW,
			int dimensionH, String description) {

		super(itemID, title, photoPath, artistName, yearCreated, reservePrice, bidLimit, date, sellerName, description);
		this.dimensionW = dimensionW;
		this.dimensionH = dimensionH;
		this.artworkType = "P";

	}

	/**
	 * Returns the width dimension for a painting.
	 * @return the width dimension for a painting.
	 */
	public int getDimensionW() {
		return dimensionW;
	}

	/**
	 *  Return the height dimension for a painting.
	 * @return the height dimension for a painting.
	 */
	public int getDimensionH() {
		return dimensionH;
	}

	/**
	 * All the information.
	 * @return the information.
	 */
	public String toString() {
		String result = super.toString();
		result += "," + this.dimensionW + "," + this.dimensionH;
		return result;
	}
	
	public String getInfo() {
		return "Title: " + this.title + "\n" +
				"Name of Artist: " + this.artistName + "\n" +
				"Year Created: " + this.yearCreated + "\n" +
				"Reserve Price: " + this.reservePrice + "\n" +
				"Date: " + this.date + "\n" +
				"Description: " + this.description + "\n" +
				"Dimensions: " + this.dimensionW + " " + this.dimensionH + "\n" +
				"Name of Seller: " + this.sellerName + "\n";
	}

}