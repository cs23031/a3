import java.io.IOException;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ListView;

/**
 * The controller for the register.fxml, where a new user can set their details
 * and create an account.
 * 
 * @author Anthony Fanias.
 *
 */
public class RegisterController {

	@FXML
	Button artataweButton;
	@FXML
	Button registerUserButton;
	@FXML
	TextField checkUsername;
	@FXML
	TextField firstName;
	@FXML
	TextField lastName;
	@FXML
	TextField checkPhone;
	@FXML
	TextField checkAddress;

	
	private FileWriter f = new FileWriter();
	
	/**
	 * Initialises the the event handlers.
	 */
	public void initialize() {
		registerUserButton.setOnAction(e -> {
			handleRegisterUser();
		});
	}

	/**
	 * Handles the event for registering user.
	 */
	private void handleRegisterUser() {
		makeNewUser();
		try {
			Parent logInPage = FXMLLoader.load(getClass().getResource("logIn.fxml"));

			Scene logInPageScene = new Scene(logInPage);

			Stage newStage = (Stage) registerUserButton.getScene().getWindow();
			newStage.setScene(logInPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void makeNewUser() {
		String username = checkUsername.getText();
		String fullname = firstName.getText() + lastName.getText();
		String phone = checkPhone.getText();
		String address = checkAddress.getText();
		String path = "D:\\payno\\Pictures\\qwer.png";
		User u = new User(username, fullname, phone, address, path);
		f.writeUserInfo(u);
	}

}
