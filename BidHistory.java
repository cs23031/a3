import java.util.ArrayList;

/**
 * This class handles the bid histories.
 * 
 * @author Josh Jones, Matt Leonard
 *
 */
public class BidHistory {
	private FileReader f = new FileReader();

	/**
	 * Gets all the artworks won by current user.
	 * @param currentUser
	 * @return the artworks won by current user.
	 */
	public String wonArtworks(String currentUser) {
		ArrayList<Integer> artworks = f.readWonHistory(currentUser);
		int i = 0;
		String s = "";
		while (i < artworks.size()) {
			s = s + ("\nArtwork won: " + f.getArtwork(artworks.get(i)).getTitle());
			i++;
		}
		return s;
	}

	/**
	 * Gets all the artworks sold by current user.
	 * @param currentUser
	 * @return
	 */
	public String SoldArtworks(String currentUser) {
		ArrayList<Integer> artworks = f.readSoldHistory(currentUser);
		int i = 0;
		String s = "";
		while (i < artworks.size()) {
			Artworks a = f.getArtwork(artworks.get(i));
			s = s + ("Artwork sold: " + a.getTitle() + "\nFor the price of: " + a.getCurrentPrice()
					+ "\nAt the time of: " + a.getDateSold() + "\n");
			i++;
		}
		return s;
	}

	/**
	 * Gets the artworks the logged in user is selling.
	 * @param currentUser
	 * @return
	 */
	public String CurrentArtworks(String currentUser) {
		ArrayList<Integer> artworks = f.readCurrentHistory(currentUser);
		String s = "";
		int i = 0;
		while (i < artworks.size()) {
			Artworks a = f.getArtwork(artworks.get(i));
			s = s + ("Artwork for sale: " + a.getTitle()) + ("\nBids: ");
			f.outBidTime(artworks.get(i));
			i++;
		}
		return s;
	}

	/**
	 * Gets a list of bids placed.
	 * @param currentUser
	 * @return
	 */
	public String buyerBids(String currentUser) {
		ArrayList<Integer> artworks = f.readCurrentHistory(currentUser);
		int i = 0;
		String s = "";
		while (i < artworks.size()) {
			Artworks a = f.getArtwork(artworks.get(i));
			s = s + ("Artwork bidding on: " + a.getTitle() + "\n" + "Bids: ");
			s = s + f.outBidTime(artworks.get(i));
			i++;
		}
		return s;
	}

}
