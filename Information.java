
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 * 
 * @author Anthony Fanias
 *
 */
public class Information {

	@FXML
	HBox hBox;
	@FXML
	ImageView photo;
	@FXML
	Label title;
	@FXML
	Label description;

	/**
	 * Construct information object.
	 */
	public Information() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("listCellItem.fxml"));
		fxmlLoader.setController(this);
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Set information for the artwork.
	 * @param artwork
	 */
	public void setInfo(Artworks artwork) {
	try {
		Image image = new Image(artwork.getPhotoPath());
		photo.setImage(image);
	} catch (Exception e) {
		e.printStackTrace();
	}
		title.setText(artwork.getTitle());
		description.setText(artwork.getDescription());

	}

	/**
	 * Set information for the user.
	 * @param user
	 */
	public void setInfo(User user) {
	try {
		Image image = new Image(user.getImagePath());
		photo.setImage(image);
	} catch (Exception e) {
		e.printStackTrace();
	}
		title.setText(user.getUserName());

	}

	/**
	 * Get HBox.
	 * @return HBox.
	 */
	public HBox getBox() {
		return hBox;
	}
}
