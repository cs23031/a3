import java.io.IOException;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ListView;

/**
 * The controller for the editProfile.fxml, where the user can edit their
 * details or go back to the main page.
 * 
 * @author Anthony Fanias.
 *
 */
public class EditProfileController {

	@FXML
	Button userLoggedIn;
	@FXML
	Button artataweButton;
	@FXML
	Button confirmButton;
	@FXML
	Button cancelButton;
	@FXML
	TextField fullName;
	@FXML
	TextField checkPhone;
	@FXML
	TextField checkAddress;
	@FXML
	Label displayUserName;

	/**
	 * Initialize the event handlers.
	 */
	public void initialize() {

		userLoggedIn.setOnAction(e -> {
			handleUserLoggedIn();
		});

		artataweButton.setOnAction(e -> {
			handleArtataweButton();
		});

		displayUserName.setText(LogInPageController.u.getUserName());
	}

	/**
	 * User logged in event handler.
	 */
	private void handleUserLoggedIn() {
		try {
			Parent profilePage = FXMLLoader.load(getClass().getResource("profile.fxml"));

			Scene profilePageScene = new Scene(profilePage);

			Stage newStage = (Stage) userLoggedIn.getScene().getWindow();
			newStage.setScene(profilePageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Artatawe event handler.
	 */
	private void handleArtataweButton() {
		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("artatawe.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) artataweButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
