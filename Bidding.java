import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 
 * @author Josh Jones, Matt Leonard
 *
 */
public class Bidding {
	
	private Double bidPlaced;
	private int counter;
	private String sellerName;
	private int itemID;
	private String currentHighestBidder;
	private String user;
	private Double highBid;
	private FileReader f = new FileReader();
	private String date;
	
	/**
	 * Constructs bidding.
	 * @param highBid
	 * @param counter
	 * @param itemID
	 * @param currentHighestBidder
	 * @param sellerName
	 * @param date
	 */
	public Bidding(Double highBid, int counter, int itemID, String currentHighestBidder, String sellerName, String date){
		this.itemID = itemID;
		this.highBid = highBid;
		this.currentHighestBidder = currentHighestBidder;
		this.counter = counter;
		this.sellerName = sellerName;
		this.date = date;
	}

	/**
	 * Returns the highest bidder.
	 * @param itemID
	 * @return the highest bidder.
	 */
	public String highestBidderCheck(int itemID) {
		return f.readBidding(itemID).currentHighestBidder;
	}

	/**
	 * Returns the current bid.
	 * @param itemID
	 * @return the current bid.
	 */	
	public Double getBid(int itemID) {
		return f.readBidding(itemID).highBid;
	}

	/**
	 *  Gets the counter and returns it.
	 * @param itemID
	 * @return counter.
	 */
	public int getCount(int itemID) {
		return f.readBidding(itemID).counter;
	}


	/**
	 *  Places a bid.
	 * @param bidPlaced
	 * @param currentUser
	 * @throws IOException
	 */
	public void placeBid(Double bidPlaced, String currentUser) throws IOException {
		this.bidPlaced = bidPlaced;
		this.user = currentUser;
		
		//Checks if the item entered is for sale
		if (this.counter < f.getArtwork(this.itemID).getBidLimit()) {
			reservePriceCheck();
		} else {
			System.out.println("ITEM SOLD");
		}

	}

	/**
	 * Checks reserve price.
	 * @throws IOException
	 */
	public void reservePriceCheck() throws IOException {
		//Checks the bid is higher than the reserve
		Double reservePrice = f.getArtwork(itemID).reservePrice;
		if (bidPlaced > reservePrice){
			bidCheck();
		} else {
			System.out.println("BID LOWER THAN RESERVE");
		}

	}

	/**
	 * Checks current bid value.
	 * @throws IOException
	 */
	private void bidCheck() throws IOException {
		//Checks the bid is higher than the current one
		Double currentPrice;
		Double e = f.readBidding(itemID).highBid;
		if (e == null) {
			currentPrice = 0.0;
		} else {
			currentPrice = f.readBidding(itemID).highBid;
		}
		if (bidPlaced > currentPrice){
			this.counter ++;

			addBid();
		} else {
			System.out.println("BID LOWER THAN HIGHEST BID");
		}

	}

	/**
	 * Adds a bid.
	 * @throws IOException
	 */
	private void addBid() throws IOException {
		FileWriter w = new FileWriter();
		LocalDateTime timeNow = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String formatDateTime = timeNow.format(formatter);
		this.date = formatDateTime;

		Artworks updatedArtwork = f.getArtwork(itemID);
		updatedArtwork.setBidsPlaced(counter);
		updatedArtwork.setCurrentPrice(bidPlaced);
		updatedArtwork.setDateSold(date);

		String artInfo = updatedArtwork.toString();

		w.updateArtwork(artInfo, itemID);
		// Writes the bid to the bidding file
		String info = (Integer.toString(itemID) + "," + Double.toString(bidPlaced) + "," + user + "," + sellerName + ","
				+ date);
		w.writeBidHistory(info);
		info = info + ("," + Integer.toString(counter));
		w.writeBidding(info, itemID);
	}

	/**
	 * Get the bid information.
	 * @return the bid information.
	 */
	public String toString() {
		return this.itemID + "," + this.highBid + "," + this.currentHighestBidder + "," + this.sellerName + ","
				+ this.date + "," + this.counter;
	}
}
