import java.io.IOException;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.control.ListView;

/**
 * The controller for the favourites.fxml, where it displays the user and
 * artatawe buttons and a list of users that were favourited.
 * 
 * @author Anthony Fanias.
 *
 */
public class FavUsersController {

	@FXML
	Button userLoggedIn;
	@FXML
	Button artataweButton;
	@FXML
	ListView<User> allUsers;
	@FXML
	ListView<User> favUsers;
	@FXML
	Label displayUserName;
	
	private FileReader f = new FileReader();
	
	public static User o;

	/**
	 * 
	 */
	public void initialize() {

		userLoggedIn.setOnAction(e -> {
			handleUserLoggedIn();
		});

		artataweButton.setOnAction(e -> {
			handleArtataweButton();
		});

		displayUserName.setText(LogInPageController.u.getUserName());

		ObservableList<User> users = FXCollections.observableArrayList (f.getAllUsers(LogInPageController.u.getUserName()));
		allUsers.setItems(users);
		allUsers.setCellFactory(new Callback<ListView<User>,javafx.scene.control.ListCell<User>>() {
			@Override
			public ListCell<User> call(ListView<User> allUsers) {
				return new ListViewCellU();
			}
		});
		allUsers.setOnMouseClicked(e -> {
			handleSelectedItem();
		});
		
		ObservableList<User> favusers = FXCollections.observableArrayList ((f.readFavUserFile(LogInPageController.u.getUserName())));
		favUsers.setItems(favusers);
		favUsers.setCellFactory(new Callback<ListView<User>,javafx.scene.control.ListCell<User>>() {
			@Override
			public ListCell<User> call(ListView<User> favUsers) {
				return new ListViewCellU();
			}
		});
		favUsers.setOnMouseClicked(e -> {
			handleSelectedItem();
		});
	}

	/**
	 * 
	 */
	private void handleUserLoggedIn() {
		try {
			Parent profilePage = FXMLLoader.load(getClass().getResource("profile.fxml"));

			Scene profilePageScene = new Scene(profilePage);

			Stage newStage = (Stage) userLoggedIn.getScene().getWindow();
			newStage.setScene(profilePageScene);
			newStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	private void handleArtataweButton() {
		try {
			Parent mainPage = FXMLLoader.load(getClass().getResource("artatawe.fxml"));

			Scene mainPageScene = new Scene(mainPage);

			Stage newStage = (Stage) artataweButton.getScene().getWindow();
			newStage.setScene(mainPageScene);
			newStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	private void handleSelectedItem() {
		if (allUsers.getSelectionModel().getSelectedItem() == null) {
			o = favUsers.getSelectionModel().getSelectedItem();
		} else {
			o = allUsers.getSelectionModel().getSelectedItem();
		}
		try {
			Parent otherPage = FXMLLoader.load(getClass().getResource("otherUser.fxml"));

			Scene otherPageScene = new Scene(otherPage);

			Stage newStage = (Stage) userLoggedIn.getScene().getWindow();
			newStage.setScene(otherPageScene);
			newStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
