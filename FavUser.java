/**
 * This class represents a Favourite user.
 * 
 * @author Matthew Payne, Abdi
 *
 */
public class FavUser extends User {

	/**
	 * FavUser constructor
	 * 
	 * @param username
	 * @param imagePath
	 */
	public FavUser(String username, String imagePath) {
		super(username, imagePath);
	}

	public String toString(){
		return this.getUserName() + "," + this.getImagePath();
	}
}
